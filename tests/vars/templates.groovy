
def conf = load "${workspace}/tests/vars/conf.groovy"


// ********** main utilities ****************


def mysql(config = [:]){
    def baseArgs = [
        name: 'mysql',
        image: 'docker.squashtest.org/forge/mysql:latest'
    ] + conf.mysql.default

    def finalArgs = baseArgs + config


    return containerTemplate(finalArgs)
}

def postgresql(config = [:]){

    def baseArgs = [
        name: 'postgres',
        image: 'postgres:9.6.12'
    ] + conf.postgresql.default

    def finalArgs = baseArgs + config

    return containerTemplate(finalArgs)
}

def squashtm(config = [:]){

    def baseArgs = [
        alwaysPullImage: true,
        name: 'squash-tm',
        image: 'docker.squashtest.org/forge/squash-tm:latest-SNAPSHOT' /*,
        resourceLimitMemory: "256Mi",*/
    ] + conf.squashtm.default

    def finalArgs = baseArgs + config

    return containerTemplate(finalArgs)
}


return this