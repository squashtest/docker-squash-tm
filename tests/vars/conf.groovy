
return [

    // ****** MYSQL conf section **********
    'mysql': [
        // Default configuration : we just use the default conf (default being, those expected by squash-tm)
        'default' : [
            envVars: [
                envVar(key: 'MYSQL_ROOT_PASSWORD',  value: 'root'),
                envVar(key: 'MYSQL_USER',           value: 'squashtm'),
                envVar(key: 'MYSQL_PASSWORD',       value: 'squashtm'),
                envVar(key: 'MYSQL_DATABASE',       value: 'squashtm')
            ]
        ],
        // fullconf : we specify all the parameters to non-default value (see definition of 'default' above)
        'fullconf' : [
            name : 'localhost',
            envVars: [
                envVar(key: 'MYSQL_ROOT_PASSWORD',  value: 'rootroot'),
                envVar(key: 'MYSQL_USER',           value: 'squashtest'),
                envVar(key: 'MYSQL_PASSWORD',       value: 'squashtest'),
                envVar(key: 'MYSQL_DATABASE',       value: 'squashtest')
            ],
            args: '--port=9876'
        ],
        // legacy : like fullconf, without changing the host and port
        'legacy' : [
            envVars: [
                    envVar(key: 'MYSQL_ROOT_PASSWORD',  value: 'rootroot'),
                    envVar(key: 'MYSQL_USER',           value: 'squashtest'),
                    envVar(key: 'MYSQL_PASSWORD',       value: 'squashtest'),
                    envVar(key: 'MYSQL_DATABASE',       value: 'squashtest')
            ]
        ],
    ],

    // ***** POSGRESQL conf section *********
    'postgresql': [
        'default' : [
            // Default configuration : we just use the default conf (default being, those expected by squash-tm)
            envVars: [
                envVar(key: 'POSTGRES_DB', value: 'squashtm'),
                envVar(key: 'POSTGRES_USER', value: 'root'),
                envVar(key: 'POSTGRES_PASSWORD', value: 'squashtm')
            ]
        ],
        // fullconf : we specify all the parameters except port to non-default value (see definition of 'default' above)
        'fullconf' : [
            name : 'localhost',
            envVars: [
                envVar(key: 'POSTGRES_DB', value: 'squashtest'),
                envVar(key: 'POSTGRES_USER', value: 'squashtest'),
                envVar(key: 'POSTGRES_PASSWORD', value: 'squashtest'),
                envVar(key: 'PGPORT', value: '5433')
            ]
        ],
        'legacy' : [
            envVars: [
                envVar(key: 'POSTGRES_DB', value: 'squashtest'),
                envVar(key: 'POSTGRES_USER', value: 'squashtest'),
                envVar(key: 'POSTGRES_PASSWORD', value: 'squashtest')
            ]
        ]        
    ],

    // ******* SQUASH-TM conf section *********
    'squashtm': [
        // noargs is the default
        'default' : [],
        // in this mode we just run the container naked without any squash process running
        'disabled' : [
            command: 'cat',
            ttyEnabled: true
        ],
        // testing the default settings : according to the install-script.sh of the docker image,
        // only the DB type and password requires to be set
        'mysql-minimal': [
            envVars: [
                envVar(key: 'SQTM_DB_TYPE', value: 'mysql'),
                envVar(key: 'SQTM_DB_PASSWORD', value: 'root')
            ]
        ],
        'mysql-fullconf': [
            envVars: [
                envVar(key: 'SQTM_DB_TYPE', value: 'mysql'),
                envVar(key: 'SQTM_DB_HOST', value: 'localhost'),
                envVar(key: 'SQTM_DB_PORT', value: '9876'),
                envVar(key: 'SQTM_DB_NAME', value: 'squashtest'),
                envVar(key: 'SQTM_DB_SCHEMA', value: 'sq-schema'),
                envVar(key: 'SQTM_DB_USERNAME', value: 'squashtest'),
                envVar(key: 'SQTM_DB_PASSWORD', value: 'squashtest')
            ]
        ],
        'mysql-legacy': [
            envVars: [
                envVar(key: 'MYSQL_ENV_MYSQL_DATABASE', value: 'squashtest'),
                envVar(key: 'MYSQL_ENV_MYSQL_USER', value: 'squashtest'),
                envVar(key: 'MYSQL_ENV_MYSQL_PASSWORD', value: 'squashtest')
            ]
        ],
        'postgres-minimal': [
            envVars: [
                envVar(key: 'SQTM_DB_TYPE', value: 'postgresql'),
                envVar(key: 'SQTM_DB_PASSWORD', value: 'squashtm')
            ]
        ],
        //note that SQTM_DB_SCHEMA is still default one 
        'postgres-fullconf': [
            envVars: [
                envVar(key: 'SQTM_DB_TYPE', value: 'postgresql'),
                envVar(key: 'SQTM_DB_HOST', value: 'localhost'),
                envVar(key: 'SQTM_DB_PORT', value: '5433'),
                envVar(key: 'SQTM_DB_NAME', value: 'squashtest'),
                envVar(key: 'SQTM_DB_SCHEMA', value: 'public'),
                envVar(key: 'SQTM_DB_USERNAME', value: 'squashtest'),
                envVar(key: 'SQTM_DB_PASSWORD', value: 'squashtest')
            ]
        ],
        'postgres-legacy': [
            envVars: [
                envVar(key: 'POSTGRES_ENV_POSTGRES_DB', value: 'squashtest'),
                envVar(key: 'POSTGRES_ENV_POSTGRES_USER', value: 'squashtest'),
                envVar(key: 'POSTGRES_ENV_POSTGRES_PASSWORD', value: 'squashtest')
            ]
        ]
        
    ]

]