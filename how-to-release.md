# Create an image

## TL;DR version

Create the image : 

    # Enter the fully qualified version, eg 1.20.0.RELEASE
    export TM_QUALIFIED_VERSION=<version>
    cd build-context
    wget -O squash-tm.tar.gz http://repo.squashtest.org/distribution/squash-tm-${TM_QUALIFIED_VERSION}.tar.gz
    
    docker build . -t docker.squashtest.org/squash-tm:${TM_QUALIFIED_VERSION} \
                --build-arg SQUASH_TM_VERSION=${TM_QUALIFIED_VERSION} \
                --build-arg DOCKER_BUILD_GIT_REVISION=$(git rev-parse HEAD) \
                --build-arg TM_ARCHIVE=squash-tm.tar.gz
                
If this is a public release, push to dockerhub :

    # strip the lifecycle qualifier 
    export TM_VERSION=${TM_QUALIFIED_VERSION%[\.-]?[A-Z][A-Z1-9]*}
    docker tag docker.squashtest.org/squash-tm:$TM_QUALIFIED_VERSION squashtest/squash-tm:$TM_VERSION
    docker push squashtest/squash-tm:$TM_VERSION

    # Optional :
    # IF this is the latest release for the branch <major>.<minor>, 
    # You should also publish it as ':<major>.<minor>-latest' 
    export TM_BRANCH_LATEST="${TM_VERSION%\.[0-9]*}-latest"
    docker tag squashtest/squash-tm:$TM_VERSION squashtest/squash-tm:$TM_BRANCH_LATEST
    docker push squashtest/squash-tm:$TM_BRANCH_LATEST 

    # Optional :
    # IF this is an absolute latest release, 
    # You should also publish it as ':latest'
    docker tag squashtest/squash-tm:$TM_VERSION squashtest/squash-tm:latest
    docker push squashtest/squash-tm:latest

## Longer version

Here are commentaries about the short procedure described above.

First, the required elements in the build context are :

* Dockerfile
* install-script.sh 
* the source TM archive

## Choose your archive
First we need to create an archive either from Nexus or repo.squashtest.org. Note that the script below downloads 
a release from the official repo but of course you can skip this step and just use one that you baked on your dev machine.

Critically though, remember to fetch the `.tar.gz` package because the `.zip` won't work. Indeed the Dockerfile includes 
the archive using the `ADD` directive, which cannot unzip zip files (but works fine with .tar.gz).  
    
    export TM_QUALIFIED_VERSION=<version>
    cd build-context
    wget -O squash-tm.tar.gz http://repo.squashtest.org/distribution/squash-tm-${TM_QUALIFIED_VERSION}.tar.gz
   

## Build the image

The Dockerfile requires three arguments : 

* SQUASH_TM_VERSION : the version of Squash-TM, including the qualifier (SNAPSHOT, RC or RELEASE)
* DOCKER_BUILD_GIT_REVISION : the git revision of this docker packaging project used for creating the image.  
* TM_ARCHIVE : the archive you just fetched

Note that the first two arguments are used as Docker labels, ie they are purely informative but not critical to the creation 
of perfectly functional images. Their main usage is for troubleshooting, eg `docker inspect <image> | grep DOCKER_BUILD_GIT_REVISION`

The `docker build` instruction creates the image then pushes it to our internal registry accordingly :

    docker build . -t docker.squashtest.org/squash-tm:${TM_QUALIFIED_VERSION} \
                --build-arg SQUASH_TM_VERSION=${TM_QUALIFIED_VERSION} \
                --build-arg DOCKER_BUILD_GIT_REVISION=$(git rev-parse HEAD) \
                --build-arg TM_ARCHIVE=squash-tm.tar.gz
    docker push docker.squashtest.org/squash-tm:TM_QUALIFIED_VERSION

## Test the image

Some automated tests are available in the `tests` folder.

Those can be used to check if the image works correctly, be it with a Postgres or a MySQL database.

The current pipeline launch a few containers, each one with a different setup. The main parameters are :

* The database type : MySQL or Postgres
* The current state of the database : Whether it needs to be fully installed or just upgraded from an old version
* The configuration (using environment vars): Minimal, complete, legacy or an insufficient configuration

## Optional : publish public releases to DockerHub 

The snippet above describes how to create an image intended for internal purposes (IT, QA etc). Once in a while one of 
theses will be a public release, which should then be promoted then pushed to DockerHub.

    # strip the lifecycle qualifier 
    export TM_VERSION=${TM_QUALIFIED_VERSION%[\.-]?[A-Z][A-Z1-9]*}
    docker tag docker.squashtest.org/squash-tm:TM_QUALIFIED_VERSION squashtest/squash-tm:$TM_VERSION
    docker push squashtest/squash-tm:$TM_VERSION

The first instruction here ensures that the lifecycle qualifier, if present, is stripped from the official tag. The image 
is then tagged as `squashtest/squash-tm` which is the official denomination on DockerHub. The push phase is self explanatory.

Most of the time - except specific cases like hot-fixing a very specific version -, you also want to publish this image as the 
latest as the latest image for that branch of Squash-TM, ie `<major>.<minor>-latest` version. Use your better judgment to decide 
whether you need it (usually you do).

    # Optional :
    # IF this is the latest release for the branch <major>.<minor>, 
    # You should also publish it as ':<major>.<minor>-latest' 
    export TM_BRANCH_LATEST="${TM_VERSION%\.[0-9]*}-latest"
    docker tag squashtest/squash-tm:$TM_VERSION squashtest/squash-tm:$TM_BRANCH_LATEST
    docker push squashtest/squash-tm:$TM_BRANCH_LATEST 

And in case this image ships the very latest Squash-TM available, also push it as such.

    # Optional :
    # IF this is an absolute latest release, 
    # You should also publish it as ':latest'
    docker tag squashtest/squash-tm:$TM_VERSION squashtest/squash-tm:latest
    docker push squashtest/squash-tm:latest
    
    
That's it. Until we fully automate the process I mean.
    